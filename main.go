// Building a basic crud with Go as learning tool
//
// The purpose of this application is manage the task, its have (ID,name, content)
//
//
//     Schemes: http
//     Host: localhost
//     BasePath: /
//     Version: 1.0.0
//
//
//     Consumes:
//     - application/json
//
//
//     Produces:
//     - application/json
//
//
//     
// swagger:meta

package main

import (
	"encoding/json" //import json
	"fmt"
	"github.com/gorilla/mux"//use the handlers of router
	"net/http" // create the http server
	"log" // Notice error that it could ocurrir
	"io/ioutil" // manage the out and in
	"strconv"
)

//Defining the data types

type task struct{
	ID int "json:ID"
	Name string "json:Name"
	Content string "json:Content"

}
// task list
type allTasks []task

// save the task list
var tasks = allTasks{
	{
		ID: 1,
		Name: "Task one",
		Content: "Some Content",
	},
}

/*Defining our endpoint's routes

1) create the get to the tasks
*/
func getTasks (w http.ResponseWriter, r *http.Request){
	// To specific info extra --> header or cabecera
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(tasks)// we return a json
}

/*Function for create one task*/
func createTask (w http.ResponseWriter, r *http.Request){
	// var that recieve user's info
	var newTask task
	// customer send the data across of a r.Body
	reqBody, err := ioutil.ReadAll(r.Body) // i can recieve the data or error
	// in case of return  error
	if err != nil{
		fmt.Fprint(w, "Insert a valid task")
	}
	//in case of return data i assign data to var newtask
	json.Unmarshal(reqBody, &newTask) // Assign the data send by user to var (newTask)

	//I set a ID to new task. Te he user only send name and content
	newTask.ID = len(tasks)+1

	tasks = append(tasks, newTask)

	// To specific info extra --> header or cabecera
	w.Header().Set("Content-Type", "application/json")
	// To send a status code for say that all is good
	w.WriteHeader(http.StatusCreated)
	//Return to customer the new task that he have created
	json.NewEncoder(w).Encode(newTask)


}

func getTask(w http.ResponseWriter, r *http.Request){
	// get the request's vars
	vars := mux.Vars(r)
	// convert string to int and it return the date else if a error
	taskID, err := strconv.Atoi(vars["id"])

	// in case of return err
	if err != nil{
		fmt.Fprint(w, "Invalid ID")
		return
	}
	// in case of return data
	for _, task := range tasks{
		if task.ID == taskID{
			// To specific info extra --> header or cabecera
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(task)
		}

	}
}

func deleteTask(w http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
	taskID, err := strconv.Atoi(vars["id"])
	if err != nil{
		fmt.Fprintf(w, "Invalid Id")
		return
	}

	for i, task := range tasks{
		if task.ID == taskID{
			tasks = append(tasks[:i], tasks[i+1:]...)
			fmt.Fprintf(w, "The task with ID %v has been remove sucessfully",taskID)
		}
	}
}

func updateTask(w http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
	taskID, err := strconv.Atoi(vars["id"])
	var updatedTask task

	if err != nil{
		fmt.Fprintf(w, "Invalid ID")

	}
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil{
		fmt.Fprintf(w, "Please Enter Valid Data")
	}
	json.Unmarshal(reqBody, &updatedTask)

	// search
	for i, task := range tasks{
		if task.ID == taskID{
			tasks = append(tasks[:i], tasks[i+1:]...)
			updatedTask.ID = taskID
			tasks = append(tasks, updatedTask)

			fmt.Fprintf(w, "The task with ID %v  has been updated sucessfully", taskID)

		}
	}
}

/*Func that define what i want to do when execute the main page*/
func indexRoute(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, "Welcome to my API")

}

func main(){
	/* Creating the router
		With the function Strict... We can restrict to user to write
		the right way the URL --> yes: /task No: /task/
	*/
	router := mux.NewRouter().StrictSlash(true)

	// When execute the main url (/) i want to do this
	router.HandleFunc("/", indexRoute)
	router.HandleFunc("/tasks", getTasks).Methods("GET")
	router.HandleFunc("/tasks", createTask).Methods("POST")
	router.HandleFunc("/tasks/{id}", getTask).Methods("GET")
	router.HandleFunc("/task/{id}", deleteTask).Methods("DELETE")
	router.HandleFunc("/tasks/{id}",updateTask).Methods("PUT")
	// We create one HTTP server and execute all time
	log.Fatal(http.ListenAndServe(":3000",router))
	/*
	* LINUX: execute next code for that server are restarting every time we do changes
	* $HOME/go/bin/CompileDaemon --command="./nameFile"
	* WINDOWS: CompileDaemon --command="nameFile.exe"
	 */
}

